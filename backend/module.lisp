(radiance:define-module #:stockshocks
  (:use #:cl #:radiance)
  (:local-nicknames (:r :radiance)
					(:u :ubiquitous)
					(:a :alexandria)))
(in-package #:stockshocks)

(setf (mconfig :radiance :interfaces :database) "i-postmodern"
      (mconfig :radiance :port) 8080
      (mconfig 'i-postmodern :default) "stockshocks"
      (mconfig 'i-postmodern :connections "stockshocks" :host) "localhost"
      (mconfig 'i-postmodern :connections "stockshocks" :port) 5432
      (mconfig 'i-postmodern :connections "stockshocks" :user) "hamza"
      (mconfig 'i-postmodern :connections "stockshocks" :pass) "hamza"
      (mconfig 'i-postmodern :connections "stockshocks" :ssl) :no
      (mconfig 'i-postmodern :connections "stockshocks" :database) "stockshocks")

(setf (u:value :salt) "ZestyDrakeBeAllIWant"
	  (u:value :redis :host) "localhost"
	  (u:value :redis :port) 6379)

;; (encode-universal-time 0 50 18 13 11 2023 -5) 
;; (encode-universal-time 0 30 16 15 11 2023 -5)
(setf (u:value :round-duration) (* 60 10) ; In seconds
	  (u:value :event-start) (- (get-universal-time) (u:value :round-duration) -10) ;; (get-universal-time)
	  (u:value :rounds) 10) ; Number of rounds

(defparameter *active-users* '())
(defparameter *inactive-users* '()) ; List of form (user-dm most-recent-share round-when-logged-out)
(defparameter *round-costs* '()) ; List of form (user-dm round round-cost)
(defparameter *round-timer-thread* nil)
(defparameter *update-db-thread* nil)

;; ;; TODO: ASK Shirakumo to not have to do this:
;; ;; Run before loading project: (setf (environment) "peteach-lms")
;; (setf (config :default) "cldb"
;;       (config :connections "cldb" :host) "localhost"
;;       (config :connections "cldb" :port) "5432"
;;       (config :connections "cldb" :user) "hamza"
;;       (config :connections "cldb" :pass) "hamza"
;;       (config :connections "cldb" :database) "cldb")

;;; Don't:
;; (setf (mconfig :peteach-lms) (mconfig :radiance))
;; (setf (mconfig :peteach-lms :interfaces :database) "i-postmodern")
