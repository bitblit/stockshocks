(in-package #:stockshocks)

(r:define-api rounds () ()
  (r:api-output (u:value :rounds)))

(r:define-api round/duration () ()
  (r:api-output (u:value :round-duration)))

(r:define-api round/current () ()
  (let ((current-round (get-current-round)))
	(if current-round
		(r:api-output current-round)
		(r:api-output -1))))

(r:define-api next-round-time () ()
  (let ((next-round (get-next-round)))
	(if next-round
		(r:api-output (universal-to-unix-time (+ (u:value :event-start)
												  (* (u:value :round-duration) next-round))))
		(r:api-output -1))))

;;; USERS
(r:define-api user/login (username password) ()
  (let ((user (check-password username password)))
	(if user
		(progn (let ((series (make-random-string))
					 (token (make-random-string)))
				 (redis:with-persistent-connection ()
				   (red:hmset username "series" series "token" token))
				 (login user)
				 (format t "Logged in user '~a' with ID '~a'~%" username (dm:id user))
				 (r:api-output (list username series token)))) 
		(api-error "Incorrect username or password."))))

(r:define-api user/cookie-login (username series token) () 
  (redis:with-persistent-connection ()
	(let ((user (ensure-user username))
		  (login-exists (red:exists username)))
	  (if login-exists
		  (if (and (str:alphanum? series) (str:alphanum? token)
				   (string= (car (red:hmget username "series")) series)
				   (string= (car (red:hmget username "token")) token))
			  (progn (let ((new-token (make-random-string)))
					   (red:hmset username "token" new-token)
					   (login user)
					   (format t "Logged in user '~a' with ID '~a'~%" username (dm:id user))
					   (r:api-output (list username series new-token))))
			  (api-error "Incorrect Cookie. YOU THEIF!!!"))
		  (api-error "No login info stored.")))))

(r:define-api user/logout (username) ()
  (let ((user (ensure-user username)))
	(a:deletef *active-users* user :test #'dm-equal)
	(logout user)
	(format t "Logged out user '~a' with ID '~a'~%" username (dm:id user))
	(r:api-output username)))

(r:define-api user/create (username password team_name) ()
  (let ((user (create-user username password team_name)))
	(format t "Created user '~a' with ID '~a'~%" username (dm:id user))
	(r:api-output username)))

(r:define-api user (user) ()
  (output-user user))

(r:define-api users () ()
  (r:api-output (mapcar #'unpasswordify-user
						(dm:get 'users (db:query :all)))))

;; No updates to SHARE, that will only be calculated in the server...
;; Optional params should not have nil as their values anyway
;; (params are stored as strings so "nil" and "" are not ignored)
(r:define-api user/update (user password &optional new_username new_password new_team) () 
  (let ((user (authenticate-user user password)))
	(when new_username
	  (validate-username new_username)
	  (setf (dm:field user "username") new_username))
	(when new_password (setf (dm:field user "password") (create-hash new_password)))
	(when new_team (setf (dm:field user "team_id") (dm:id (ensure-team new_team))))
	(output-user (dm:save user))))

(r:define-api user/delete (user password) ()
  (let* ((user (authenticate-user user password)))
	(dm:delete user)
	;; (recalculate-shares (get-user-team user))
	(format t "Deleted user '~a'~%" user)
	(output-user user)))

(r:define-api user/buy (user stock amount) ()
  (let ((user (ensure-user (if (str:numeric? user) (parse-integer user) user)))
		(stock (ensure-stock stock)))
	(cond ((not (user-active? user)) (r:api-error "Inactive users cannot buy stocks."))
		  ((not (str:numeric? amount)) (r:api-error "The amount of stocks to buy must be a number."))
		  (t (r:api-output (buy-stock stock user (parse-integer amount)))))))

(r:define-api user/sell (user stock amount) ()
  (let ((user (ensure-user user))
		(stock (ensure-stock stock)))
	(cond ((not (user-active? user)) (r:api-error "Inactive users cannot sell stocks."))
		  ((not (str:numeric? amount)) (r:api-error "The amount of stocks to sell must be a number."))
		  (t (r:api-output (sell-stock stock user (parse-integer amount)))))))

;;; TEAMS
(r:define-api team/create (name) ()
  (format t "Created team '~a' with ID '~a'~%" name (dm:id (create-team name)))
  (r:api-output name))

(r:define-api team (team) ()
  (r:api-output (ensure-team team)))

(r:define-api user-team (user) ()
  (r:api-output (get-user-team user)))

(r:define-api teams () ()
  (r:api-output (dm:get 'teams (db:query :all))))


;; No updates to Balance? That will only be calculated in the server...
(r:define-api team/update (team new_name) () 
  (let ((team (ensure-team team)))
	(validate-team-name new_name)
	(setf (dm:field team "name") new_name)
	(api-output (dm:save team))))

;;; WAY too much power...
;; (r:define-api team/delete (name) ()
;;   (let ((team (ensure-team name)))
;; 	(dm:delete team)
;; 	(format t "Deleted team '~a' with ID '~a'~%" name (dm:id team))
;; 	(r:api-output name)))

;;; STOCKS
(r:define-api stock/create (name price) ()
  (format t "PRICE: ~a~%" price)
  (format t "Created stock '~a' with ID '~a'~%" name (dm:id (create-stock name price)))
  (r:api-output name))

(r:define-api stock (stock) ()
  (r:api-output (ensure-stock stock)))

(r:define-api stocks () ()
  (r:api-output (dm:get 'stocks (db:query :all))))

(r:define-api stock/update (stock &optional new_name new_price) () 
  (let ((stock (ensure-stock stock)))
	(when (and (null new_name) (null new_price)) (api-error "Atleast new_name or new_price must be passed."))
	(when new_name
	  (validate-stock-name new_name)
	  (setf (dm:field stock "name") new_name))
	(when new_price 
	  (validate-stock-price new_price)
	  (setf (dm:field stock "price") (parse-integer new_price)))
	(r:api-output (dm:save stock))))

(r:define-api stock/delete (name) ()
  (let ((stock (ensure-stock name)))
	(dm:delete stock)
	(format t "Deleted stock '~a' with ID '~a'~%" name (dm:id stock))
	(r:api-output name)))

(r:define-api user/owned (user stock) ()
  (let* ((user-id (dm:id (ensure-user user)))
		 (stock-id (dm:id (ensure-stock stock)))
		 (user-stock (dm:get-one 'user-stocks (db:query (:and (:= 'user user-id)
															  (:= 'stock stock-id))))))
	(r:api-output (if user-stock (dm:field user-stock "owned") 0))))
