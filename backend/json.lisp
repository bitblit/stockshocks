(in-package #:stockshocks)

(defgeneric serialize (o)
  (:method ((o string)) o)
  (:method ((o real)) o)
  (:method ((o symbol)) o)
  (:method ((o list))
    (mapcar #'serialize o))
  (:method ((o vector))
    (map 'vector #'serialize o))
  (:method ((o hash-table))
    (loop with n = (make-hash-table :test (hash-table-test o))
          for k being the hash-keys of o
          for v being the hash-values of o
          do (setf (gethash (serialize k) n)
                   (serialize v))
          finally (return n)))
  (:method ((o condition))
    (loop with n = (make-hash-table)
          for d in (c2mop:class-slots (class-of o))
          for s = (c2mop:slot-definition-name d)
          for v = (slot-value o s)
          do (setf (gethash (serialize s) n)
                   (serialize v))
          finally (return n)))
  (:method ((o T))
    (handler-case
        (api-serialize o)
      (api-unserializable-object (e)
        (declare (ignore e))
        (princ-to-string o)))))

(define-api-format json (object)
  (setf (content-type *response*) "application/json; charset=utf-8")
  ;; ;; Allow CORS
  ;; (setf (header "Access-Control-Allow-Origin") "*")
  ;; (setf (header "Access-Control-Allow-Methods") "GET,POST") ;GET, POST, OPTIONS
  ;; (setf (header "Access-Control-Allow-Headers")
  ;;       "DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range")
  ;; (setf (header "Access-Control-Max-Age") "1728000") ; Tell client that pre-flight is valid for 20 days
  (with-output-to-string (stream)
    (cl-json:encode-json (serialize object) stream)))

(setf *default-api-format* "json")
