(in-package #:stockshocks)

(r:define-trigger db:connected ()
  (db:create 'teams '((name (:varchar 32))
					  (balance :integer))) ; In paisas/cents
  (db:create 'users '((username (:varchar 32))
					  (password (:varchar 128))
					  (team_id (:id teams))
					  (share :float)))
  (db:create 'stocks '((name (:varchar 32))
					   (price :integer)))
  (db:create 'user-stocks '((user (:id users))
							(stock (:id stocks))
							(owned :integer)))
  (db:create 'stock-history '((stock (:id stocks))
							  (round :integer)
							  (price :integer)))
  (unless (redis:connected-p)
	(redis:connect :host (u:value :redis :host) :port (u:value :redis :port))))

(defun get-user (user-ish)
  "Get the user datamodel from username/id/whatever"
  (typecase user-ish
	(dm:data-model user-ish)
	(db:id (dm:get-one 'users (db:query (:= '_id user-ish))))
	(T (let ((user (dm:get-one 'users (db:query (:= 'username user-ish)))))
		 (if user user (when (str:numeric? user-ish)
						 (get-user (parse-integer user-ish))))))))

(defun ensure-user (user-ish)
  "GET-USER but errors if it does not exist"
  (let ((user (get-user user-ish)))
	(if (null user) (api-error "User '~a' does not exist." user-ish) user)))

(defun check-password (user-ish password)
  (let ((user (get-user user-ish)))
	(when (and user (string= (dm:field user "password") (create-hash password))) user)))

;; I could show "Incorrect USERNAME OR password..." but would then have to delete
;; check-password :(
(defun authenticate-user (user-ish password)
  "Like CHECK-PASSWORD but errors if the user does not exist by using ENSURE-USER"
  (let ((user (ensure-user user-ish)))
	(if (string= (dm:field user "password") (create-hash password))
		user
		(api-error "Incorrect password for user '~a'" (dm:field user "username")))))

(defun unpasswordify-user (user-ish)
  (let* ((user (ensure-user user-ish))
		 (field-table (slot-value user 'dm::field-table)))
	(remhash "password" field-table)
	user))

(defun output-user (user-ish)
  (r:api-output (unpasswordify-user user-ish)))

(defun get-team (team-ish)
  "Get the team datamodel from team/id/whatever"
  (typecase team-ish
	(dm:data-model team-ish)
	(db:id (dm:get-one 'teams (db:query (:= '_id team-ish))))
	(T (let ((team (dm:get-one 'teams (db:query (:= 'name team-ish)))))
		 (if team team (when (str:numeric? team-ish)
						 (get-team (parse-integer team-ish))))))))

(defun ensure-team (team-ish)
  "GET-TEAM but errors if it does not exist"
  (let ((team (get-team team-ish)))
	(if (null team) (api-error "Team '~a' does not exist." team-ish) team)))

(defun get-user-team (user-ish)
  "Get the user's team"
  (get-team (dm:field (ensure-user user-ish) "team_id")))

(defun get-active-team-members (team-ish)
  (let* ((team-id (dm:id (ensure-team team-ish)))
		 (users (dm:get 'users (db:query (:= 'team_id team-id)))))
	(intersection users *active-users* :test #'dm-equal)))

(defun get-stock (stock-ish)
  "Get the stock datamodel from stockname/id/whatever"
  (typecase stock-ish
	(dm:data-model stock-ish)
	(db:id (dm:get-one 'stocks (db:query (:= '_id stock-ish))))
	(T (let ((stock (dm:get-one 'stocks (db:query (:= 'name stock-ish)))))
		 (if stock stock (when (str:numeric? stock-ish)
						 (get-stock (parse-integer stock-ish))))))))

(defun ensure-stock (stock-ish)
  "GET-stock but errors if it does not exist"
  (let ((stock (get-stock stock-ish)))
	(if (null stock) (api-error "Stock '~a' does not exist." stock-ish) stock)))

(defun get-stock-history (stock-history-ish)
  "Get the stock-history datamodel from stockname/id/whatever"
  (typecase stock-history-ish
	(dm:data-model stock-history-ish)
	(db:id (dm:get-one 'stock-history (db:query (:= '_id stock-history-ish))))
	(T (let ((stock-history (dm:get-one 'stock-history (db:query (:= 'name stock-history-ish)))))
		 (if stock-history stock-history (when (str:numeric? stock-history-ish)
										   (get-stock-history (parse-integer stock-history-ish))))))))

(defun ensure-stock-history (stock-history-ish)
  "GET-STOCK-HISTORY but errors if it does not exist"
  (let ((stock-history (get-stock stock-history-ish)))
	(if (null stock-history)
		(api-error "Stock History '~a' does not exist." stock-history-ish)
		stock-history)))

(defun create-hash (password)
  (cryptos:pbkdf2-hash password (u:value :salt)))

(defun dm-equal (x y) (= (dm:id x) (dm:id y)))
(defun user-active? (user) (member (get-user user) *active-users* :test #'dm-equal))

(defun validate-username (username)
  (cond ((get-user username)            (api-error "Username already exists."))
		((str:empty? username)          (api-error "Username cannot be empty."))
		((str:contains? " " username)   (api-error "Username must not contain whitespaces."))
		((not (str:alphanum? username)) (api-error "Username must contain only alphanumeric characters."))
		((> (length username) 32)       (api-error "Username must not exceed 32 characters.")))
  username)

(defun create-user (username password team-name)
  (let* ((user (dm:hull 'users)))
	(setf (dm:field user "username") (validate-username username)
		  (dm:field user "password") (create-hash password)
		  (dm:field user "team_id") (dm:id (ensure-team team-name))
		  (dm:field user "share") 0)
	(dm:insert user)))

(defun sufficient-balance? (user amount)
  (let ((team-balance (dm:field (get-user-team user) "balance"))
		(user-share (dm:field user "share"))
		(round-cost (reduce #'+ (mapcar #'third 
										(remove-if-not (lambda (x)
														 (and (dm-equal user (first x))
															  (= (get-current-round) (second x))))
													   *round-costs*)))))
	(<= (+ amount (if round-cost round-cost 0)) (* team-balance user-share))))

(defun get-user-balance (user-ish)
  (let ((user (ensure-user user-ish)))
	(+ (* (dm:field user "share") (dm:field (get-user-team user) "balance"))
		(loop :for user-stock :in (dm:get 'user-stocks (db:query (:= 'user (dm:id user))))
			  :sum (* (dm:field user-stock "owned")
					  (dm:field (ensure-stock (dm:field user-stock "stock")) "price"))))))

(defun buy-stock (stock user amount)
  "AMOUNT must be an integer"
  ;; let* stock-price 
  ;; ;; (cost (+ (* amount stock-price) (get-user-balance user))))
  (when (not (sufficient-balance? (ensure-user user) (* amount (dm:field stock "price"))))
	(api-error "Insufficient balance to buy stocks."))
  (let* ((user-id (dm:id user))
		 (stock-id (dm:id (ensure-stock stock)))
		 (user-stocks (dm:get-one 'user-stocks (db:query (:and (:= 'user user-id) (:= 'stock stock-id)))))
		 (user-team (get-user-team user)))
	(if user-stocks
		(progn (setf (dm:field user-stocks "owned")
					 (+ amount (dm:field user-stocks "owned")))
			   ;; (dm:save user-stocks)
			   )
		(let ((user-stocks (dm:hull 'user-stocks)))
		  (setf (dm:field user-stocks "user") user-id
				(dm:field user-stocks "stock") stock-id
				(dm:field user-stocks "owned") amount)
		  (dm:insert user-stocks)))
	;; (dm:save user-team)
	(push (list user (get-current-round) (* amount (dm:field stock "price")))
		  *round-costs*)))

(defun sell-stock (stock user amount)
  "STOCK and USER must exist in the database, arguments must be sanitized & AMOUNT must be an integer"
  (let* ((user-stocks (dm:get-one 'user-stocks (db:query (:and (:= 'user (dm:id user))
															   (:= 'stock (dm:id stock)))))))
	(cond ((not user-stocks) (api-error "Cannot sell stocks when user owns none."))
		  ((> amount (dm:field user-stocks "owned"))
		   (api-error "Cannot sell more stocks than the user owns."))
		  (t (setf (dm:field user-stocks "owned")
				   (- (dm:field user-stocks "owned") amount))
			 ;; (dm:save user-stocks)
			 ))))

;; (defun recalculate-shares (team)
;;   "If TEAM is not nil, update each user's share according to team size and *ACTIVE-USERS*.
;; Otherwise, do nothing."
;;   (when team
;; 	(let* ((team-id (dm:id (ensure-team team)))
;; 		   (users (dm:get 'users (db:query (:= 'team_id team-id))))
;; 		   (team-size (length (intersection users *active-users* :test #'dm-equal)))
;; 		   (new-share (if (< (print team-size) 1)
;; 						  0 ;(api-error "Cannot recalculate shares with zero team members active.")
;; 						  (/ 1 team-size))))
;; 	  ;; (print (list team-id new-share))
;; 	  (loop :for user :in users
;; 			:when (user-active? user)
;; 			  :do (setf (dm:field user "share") new-share)
;; 				  (dm:save user)))))

(defun validate-team-name (name)
  (cond ((get-team name)            (api-error "A team with this name already exists."))
		((str:empty? name)          (api-error "The team's name cannot be empty."))
		((str:contains? " " name)   (api-error "The team's name not contain whitespaces."))
		((not (str:alphanum? name)) (api-error "The team's name must only contain alphanumeric characters."))
		((> (length name) 32)       (api-error "The team's name must not exceed 32 characters.")))) 

(defun create-team (name)
  (validate-team-name name)
  (let ((team (dm:hull 'teams)))
	(setf (dm:field team "name") name
		  (dm:field team "balance") 10000)  ;$10k in cents/paisas whatever
	(dm:insert team)))

(defun validate-stock-name (name)
  (cond ((get-stock name)           (api-error "A stock with this name already exists."))
		((str:empty? name)          (api-error "The stock's name cannot be empty."))
		((not (str:alphanum? name)) (api-error "The stock's name must only contain alphanumeric characters."))
		((> (length name) 32)       (api-error "The stock's name must not exceed 32 characters."))))

(defun validate-stock-price (price)
  (when (not (str:numeric? price)) (api-error "The stock's price must be an integer")))

(defun create-stock (name price)
  (validate-stock-name name)
  (validate-stock-price price)
  (let ((stock (dm:hull 'stocks)))
	(setf (dm:field stock "name") name
		  (dm:field stock "price") (parse-integer price)) ;$21 in cents/paisas whatever
	(dm:insert stock)))  

(defun delete-team (name)
  (dm:delete (ensure-team name)))

(defun logout (user-ish)
  (let* ((user (ensure-user user-ish)))
	(pushnew (list user (dm:field user "share") (get-current-round)) *inactive-users*)
	(setf *active-users* (remove user *active-users* :test #'dm-equal))
	(loop :for member :in (get-active-team-members (get-user-team user))
		  :do (setf (dm:field member "share")
					(/ (dm:field member "share")
					   (- 100 (dm:field user "share"))))
			  ;; (dm:save member)
		  )
	(setf (dm:field user "share") 0)
	;; (dm:save user)
	))

(defun login (user-ish)
  (let* ((user (ensure-user user-ish))
		 ;; Seperated INTENTIONALLY as we use push and want latest
		 (record (when (get-current-round) 
				   (or (find (get-current-round) *inactive-users*
							 :test (lambda (x y) (= x (third y))))
					   (find (1- (get-current-round)) *inactive-users*
							 :test (lambda (x y) (= x (third y)))))))
		 (active-team-members (get-active-team-members (get-user-team user))))
	(if record
		(setf (dm:field user "share") (second record))
		(progn
		  (let* ((active-team-size (length active-team-members))
				 (default-share (if (zerop active-team-size) 1 (/ 1 active-team-size))))
			(setf (dm:field user "share") default-share)
			(loop :for member :in active-team-members
				  :do (setf (dm:field member "share")
							(- (dm:field member "share") (/ default-share active-team-size)))
					  ;; (dm:save member)
				  ))))
	;; (dm:save user)
	(pushnew user *active-users* :test #'dm-equal)
	(format t "[NONAPI] User '~a' logged in w/ share ~a in a team of ~a~%" (dm:field user "username") (dm:field user "share")
		   (length (get-active-team-members (get-user-team user))))))

(defun get-pakistan-time ()
  (multiple-value-bind (second minute hour date month year dow daylight std-time timezone)
	  (decode-universal-time (get-universal-time) -5)
	(declare (ignore dow daylight std-time timezone))

	(encode-universal-time second minute hour date month year -5)))

(defun get-next-round ()
  "Returns the round to generate a timer for. If all rounds have ended, NIL is returned."
  (loop :for round :from 1 :to (u:value :rounds)
		:when (>= (+ (u:value :event-start) (* round (u:value :round-duration)))
				  (get-pakistan-time))
		  :return round))

(defun get-current-round ()
  "Returns the ongoing round. If all rounds have ended, NIL is returned."
  (let ((next-round (get-next-round)))
	(when next-round (1- next-round))))

;; (defun start-round (round)
;;   (format t "STARTING NEXT ROUND... ~a~%" round)
;;   (force-output)
;;   ;; Update all active user shares
;;   (loop :for user :in (dm:get 'users (db:query :all))
;; 		:when (not (user-active? user))
;; 		  :do (setf (dm:field user "share") 0)
;; 			  (dm:save user))
;;   (loop :for user :in *active-users*
;; 		:do (let ((user (ensure-user user)))
;; 			  (if (dm:get 'user-stocks (db:query (:= 'user (dm:id user))))
;; 				  (setf (dm:field user "share")
;; 						(/ (get-user-balance user) 100))
;; 				  (let* ((team-id (dm:id (get-user-team user)))
;; 						 (users (dm:get 'users (db:query (:= 'team_id team-id))))
;; 						 (team-size (length (intersection users *active-users* :test #'dm-equal)))
;; 						 (new-share (if (< (print team-size) 1)
;; 										0
;; 										(/ 1 team-size))))
;; 					(loop :for user :in users
;; 						  :when (user-active? user)
;; 							:do (setf (dm:field user "share") new-share)
;; 								(dm:save user))))))
;;   ;; Update all stock prices
;;   (let ((round-stock-histories (dm:get 'stock-history (db:query (:= 'round round)))))
;; 	(loop :for stock-history :in round-stock-histories
;; 		  :do (let ((stock (ensure-stock (dm:field stock-history "stock"))))
;; 				(setf (dm:field stock "price") (dm:field stock-history "price"))
;; 				(dm:save stock)))))

(defun start-round (round)
  (format t "STARTING NEXT ROUND... ~a~%" round)
  (loop :for user :in *active-users* ;(dm:get 'users (db:query :all))
		:do (if (not (= round 1))
				(let* ((profit 
						 (loop :for user-stock :in (dm:get 'user-stocks (db:query (:= 'user (dm:id user))))
							   :sum (let* ((stock (ensure-stock (dm:field user-stock "stock")))
										   (old-stock (dm:get-one 'stock-history 
																  (db:query (:and (:= 'round (1- round))
																				  (:= 'stock (dm:id stock))))))
										   (new-stock (dm:get-one 'stock-history 
																  (db:query (:and (:= 'round round)
																				  (:= 'stock (dm:id stock))))))
										   (old-capital (* (dm:field user-stock "owned") (if old-stock (dm:field old-stock "price") 0)))
										   (new-capital (* (dm:field user-stock "owned") (if new-stock (dm:field new-stock "price") 0))))
									  (- old-capital new-capital))))
					   (team (get-user-team user))
					   (active-team-size (length (get-active-team-members team)))
					   (old-bank (dm:field team "balance"))
					   (old-share (dm:field user "share"))
					   (initial-capital (* old-bank old-share)))
				  (setf (dm:field user "share")
						(if (<= (+ profit old-bank) 0)
							(if (zerop active-team-size)
								1
								(/ 1 active-team-size))
							(/ (+ profit initial-capital)
							   (+ profit old-bank))))
				  ;; (dm:save user)
				  (setf (dm:field team "balance") (+ profit old-bank))
				  ;; (dm:save team)
				  )
				(let* ((team (get-user-team user))
					   (active-team-size (length (get-active-team-members team))))
				  (setf (dm:field user "share") (if (zerop active-team-size)
													1
													(/ 1 active-team-size)))
				  ;; (dm:save user)
				  (setf (dm:field team "balance") 10000)
				  ;; (dm:save team)
				  )))

  ;; Update all stock prices
  (let ((round-stock-histories (dm:get 'stock-history (db:query (:= 'round round)))))
	(loop :for stock-history :in round-stock-histories
		  :do (let ((stock (ensure-stock (dm:field stock-history "stock"))))
				(setf (dm:field stock "price") (dm:field stock-history "price"))
				(dm:save stock)))))

(defun generate-round-timer ()
  (bt:make-thread (lambda ()
					(when (get-next-round)
					  (loop :for round :from (get-next-round) :to (u:value :rounds)
							:do (sleep (- (+ (u:value :event-start) (* (u:value :round-duration) round))
										   (get-pakistan-time)))
								(format t "Niggaasaa~%")
								(start-round round)))
					nil)))

(defun generate-update-db-thread ()
  (bt:make-thread (lambda ()
					(loop
					  (sleep 0.2)
					  (db:with-transaction ()
						(loop :for team :in (dm:get 'teams (db:query :all))
							  :do (dm:save team))
						(loop :for user :in (dm:get 'users (db:query :all))
							  :do (dm:save user))
						(loop :for user-stock :in (dm:get 'user-stocks (db:query :all))
							  :do (dm:save user-stock)))))))

;; (defun generate-active-users-thread ()
;;   (bt:make-thread (lambda ()
;; 					(loop :for user :in *active-users*
;; 						  :when ()))))

(r:define-trigger startup ()
  ;; (when (and *active-users-thread* (bt:thread-alive-p *active-users-thread*))
  ;; 	(bt:destroy-thread *active-users-thread*))
  ;; (setf *active-users-thread* (generate-active-users-thread))
  (when (and *round-timer-thread* (bt:thread-alive-p *round-timer-thread*))
	(bt:destroy-thread *round-timer-thread*))
  (setf *round-timer-thread* (generate-round-timer))
  (when (and *update-db-thread* (bt:thread-alive-p *update-db-thread*))
	(bt:destroy-thread *update-db-thread*))
  (setf *update-db-thread* (generate-update-db-thread)))

(defun setup-db ()
;;  (loop :for i :in
;;		'(("Dynamic entrepreneurs" "Zubia Mohsin" "Fatima Anwar" "Falah Shiekh" "Wajiha Tariq" "Abeer Fahad")
;;		  ("The Memon Sharks (TMS)" "Ahmed Patel" "Huzaifa Abid" "Laksh Kumar" "Hanzala Nadeem" "Subhan Khan")
;;		  ("Balls" "Danyal Sufyan Ahmed" "Shayan Abbasi" "Zonah Babar" "Alyan jameel" )
;;		  ("Icarus" "Izza Ilyas" "Asfiya Salman" "Ansharah Adeel" "Ushba Faizan" "Maryum Babar")
;;		  ("TW" "Myra Ali" "Umer Khan" "Zahraa" "Ayra Ali" "Muhammad Bin Salman")
;;		  ("Innovative Ventures" "Ruhaab Hassan" "Barerah Shahbaz" "Aaira Mansoor" "Adeera Kamran" "Eshaal Ahmed")
;;		  ("EAGLES" "Kanza Amir" "Zakiya Syed" "Adeena Hassan" "Alvirah Rahim" "Aimon Shafiq")
;;		  ("White Rock" "Muhammad Ibrahim Najam" "Safeeullah Khan" "Muhammad Taha Javed" "Farzaam Aamir" "Abdullah Abid")
;;		  ("The Visionary Mavericks" "Aayan Aamir" "Anas Faisal Siddiqui" "Syed Aayan" "Zuhair Khan" )
;;		  ("Vision Enterprise" "Ukasha Ahmed" "Shayan Ahmed" "Saifullah Salman" "Yousuf Hafeez" "Mohsin Ajaz")
;;		  ("Futurists" "Hasnain Hamid" "Rafay ansari" "Omer humayun" "Haseeb chughtai" "Umar zuberi")
;;		  ("Team Matrix" "Abdullah" "Muzammil Ali" "Abdul Aziz Lakha" "Muhammad Umar Abbas" "Ameen Usman")
;;		  ("Business Titans" "Aashir Qureshi" "Ikrimah Khanani" "Aayan Anis" "Huzaifa Patel" "Hesham Qureshi")
;;		  ("Team Caffeine" "Mahd Ashraf" "Zaid Khan" "Hussain Amir" "Mustafa Ilyas Khan")
;;		  ("Mavericks" "Baneeta masroor" "Zoya imran" "Leena mirza" "Jumana rangwala" "Arhama shaikh")
;;		  ("Team inferno" "Amal Aqeel" "Laiba syed" "Shanzay Salman" "Widad Khan" "Momina Hussain")
;;		  ("Prosperity Architects" "Romaisa Shoaib" "Khulood Faisal" "Ayesha Khan" "Maryam Aftab" "Syeda Fatima Zohair")
;;		  ("Cosmom" "Syed Huzaifa Ahmed" "Humayl Sheikh" "Owais Imran" "Tanzeel Tahir" "Taizoon Quaid Johar")
;;		  ("Hustle Five" "Saqib Amir" "Zain Farhan" "Sami Ahsan" "Shafin Imam" "Omer Salman")
;;		  ("Shaheens" "ABDULRAFAY JADOON" "Ibrahim khan" "Aaiz jadoon" "Abdul Rafay jadoon" "Ammar siddiqui")
;;		  ("Mission Possible" "Hammad Shamsi" "Abdullah Siddiqui" "M.Ali Rizvi" "M.Ahmed Khan" "Rayyan Ahmed")
;;		  ("Divide Et Impera" "Fatema Haider" "Sarah Mawani" "Aimen Husnain" "Laiba Asif")
;;		  ("The Magnates" "Maryam Anwar" "Areeba Khan" "Rumaisa Muzammil" "Laiba Quddusi" )
;;		  ("Profit Prospectors" "Usman Zunorain" "Ibrahim" "Syed Shaheer Inam" "M Shaheer Sheikh" "Khizar Alvi")
;;		  ("Synergistix" "Malick Abdullah" "Shaheer Ahmed Khan" "Ebad Ullah" "Kabeer Javed" "Sudaim Ahmed")
;;		  ("Aspire" "Hadi Saad" "Syed Arham Ali Bukhari" "Umar Fareed" "Ayan Khurram" "Sudais Sheikh")
;;		  ("Trailblazers" "Javeria Tariq" "Zayna Azam Mirza" "Shafia Siddiqui" "Huda Zafar" "Muzammil Shah")
;;		  ("The Sharks" "Syed Moaz Hussain" "Ayaan Shahdat" "Raiyaan Saadi" "Syed Ahmed" "Ali Zafar")
;;		  ("hustlers crew" "Haris Farooq" "Ehson Tariq" "Fawad Ahmed" "Hassan Muzaffar" )
;;		  ("The Neurotics" "Zainab Zubair" "Kainat Salman" "Tooba Haq" "Amna Zeeshan Qureshi")
;;		  ("Highbrow2023" "Syed Muhammad Ashir Ali" "Eman Abid" "Hafsa Ahmad" "Nida Irshad" )
;;		  ("Quademme" "Yumna Ishaq" "Afrah" "Maria Inayat" "Hasanah Hamid")
;;		  ("InnoVisionaries" "Abdul rafay" "Rubaid faisal" "Ibraheem Zaheem" "Shafay naeem" "Yousuf alvi")
;;		  ("business mavericks" "Aniqa shakil" "Nabiha Zia" "Farwa Zehra" "Alavia imran" )
;;		  ("NIXOR OLS" "Aaminah Jawed Japanwala" "Kunwar Mohammad Aaly Nouman" "Arwa Fahad Mandavia" "Ushay Zohair Poonawala" "Eshal Qazi")
;;		  ("Sceptre gold" "Ryaan siddiqui" "Saad habib" "Fiza shahzad" "Mahnoor magsi" "Azzam ahmed2")
;;		  ("Sceptre White" "Azzam Ahmed" "Harmain Imran" "Fiza Shahzad2" "Siraj uddin" "Abiha ")
;;		  ("Odyssey" "Ahmed Raza" "Muneeb Qaiser" "Fariha Nazeer" "Dua Ameen" "Muneeb Ahmed")
;;		  ("Genesis" "Sania Maham" "Tayyaba Waqar" "Manahil Asif" "Dilawar Shams" "Syed Sarib Ali")
;;		  ("Zenith" "Emaan Farukh" "Sumbul Faisal" "Subhan Faisal" "Ayesha Kanwal" "Hania Kashif")
;;		  ("Sceptre green" "Ali Imran Shaikh" "Aina khan Marri" "Abdul wasay" "Wali Muhammad Amjad" )
;;		  ("Sceptre black official" "Shayan uddin" "Kasak kumari" "Raja taha" "Hassan" )
;;		  ("Stratagem Masters" "Khubaib Zubair" "Abdul Hadi" "Muhammad rayyan raheel" "Rafay yameen" )
;;		  ("Team Tycoon Gladiators" "Ahyaan Anwar" "Saif Ali" "Shavez Zeeshan" "Haseeb" )
;;		  ("The Innovators" "Maria Jabbar" "Sohaib Salman" "Ammar fahim" "Humza fahim"))
;;		:do (let* ((team-name (str:pascal-case (car i)))
;;				   (users (cdr i)))
;;			  (create-team team-name)
;;			  (loop :for username :in users 
;;					:do (let ((pass (str:substring 2 -2 (dex:get "https://random-word-api.herokuapp.com/word"))))
;;						  (format t "User ~a, team ~a, pass ~a~%" (str:pascal-case username) team-name pass)
;;						  (force-output)
;;						  (create-user (str:pascal-case username) pass team-name)))))

  (loop :for stock :in '(("Crimson            "      65)
					   ("Gt ammunitions  	  "	 40   )
					   ("Pharmevo           "      70 )
					   ("Gear automobile    "      0  )
					   ("Golden oil company "      45 )
					   ("Hasco              "      55 )
					   ("Gobi Foods         "      60 )
					   ("Texon Cement       "      50 )
					   ("Hills bay seafood  "      55 )
					   ("Ace Pharmaceutics  "      50 ))
		:do (create-stock (str:pascal-case (str:trim (first stock))) (write-to-string (second stock))))

  (loop :for stock :in '(("Crimson            "      65 1)
					   ("Gt ammunitions  	  "	 40   1)
					   ("Pharmevo           "      70 1)
					   ("Gear automobile    "      0  1)
					   ("Golden oil company "      45 1)
					   ("Hasco              "      55 1)
					   ("Gobi Foods         "      60 1)
					   ("Texon Cement       "      50 1)
					   ("Hills bay seafood  "      55 1)
					   ("Ace Pharmaceutics  "      50 1)
					   ("Crimson            "      40 2)
					   ("Gt ammunitions     "      30 2)
					   ("Pharmevo           "      50 2)
					   ("Gear automobile    "      0  2)
					   ("Golden oil company "      30 2)
					   ("Hasco              "      35 2)
					   ("Gobi Foods         "      50 2) 
					   ("Texon Cement       "      40 2)
					   ("Hills bay seafood  "      40 2)
					   ("Ace Pharmaceutics  "      30 2)
					   ("Crimson            "      30 3)
					   ("Gt ammunitions     "      30 3)
					   ("Pharmevo           "      50 3)
					   ("Gear automobile    "      0  3)
					   ("Golden oil company "      20 3)
					   ("Hasco              "      35 3)
					   ("Gobi Foods         "      50 3) 
					   ("Texon Cement       "      20 3)
					   ("Hills bay seafood  "      40 3)
					   ("Ace Pharmaceutics  "      45 3)
					   ("Crimson            "      30 4)
					   ("Gt ammunitions     "      30 4)
					   ("Pharmevo           "      50 4)
					   ("Gear automobile    "      0  4)
					   ("Golden oil company "      20 4)
					   ("Hasco              "      35 4)
					   ("Gobi Foods         "      50 4) 
					   ("Texon Cement       "      20 4)
					   ("Hills bay seafood  "      40 4)
					   ("Ace Pharmaceutics  "      45 4)
					   ("Crimson            "      25 5)
					   ("Gt ammunitions     "      60 5)
					   ("Pharmevo           "      60 5)
					   ("Gear automobile    "      0  5)
					   ("Golden oil company "      20 5)
					   ("Hasco              "      20 5)
					   ("Gobi Foods         "      60 5) 
					   ("Texon Cement       "      30 5)
					   ("Hills bay seafood  "      60 5)
					   ("Ace Pharmaceutics  "      20 5)
					   ("Crimson            "      10 6)
					   ("Gt ammunitions     "      60 6)
					   ("Pharmevo           "      50 6)
					   ("Gear automobile    "      0  6)
					   ("Golden oil company "      10 6)
					   ("Hasco              "      20 6)
					   ("Gobi Foods         "      50 6) 
					   ("Texon Cement       "      30 6)
					   ("Hills bay seafood  "       40 6)
					   ("Ace Pharmaceutics  "      20 6)
					   ("Crimson            "      10 7)
					   ("Gt ammunitions     "      70 7)
					   ("Pharmevo           "      40 7)
					   ("Gear automobile    "      0  7)
					   ("Golden oil company "      10 7)
					   ("Hasco              "      0  7)
					   ("Gobi Foods         "      50  7) 
					   ("Texon Cement       "      30  7)
					   ("Hills bay seafood  "      40  7)
					   ("Ace Pharmaceutics  "      0   7)
					   ("Crimson            "      5   8)
					   ("Gt ammunitions     "      0   8)
					   ("Pharmevo           "      60  8)
					   ("Gear automobile    "      0   8)
					   ("Golden oil company "      0   8)
					   ("Hasco              "      0   8)
					   ("Gobi Foods         "      50  8) 
					   ("Texon Cement       "      0   8)
					   ("Hills bay seafood  "      40  8)
					   ("Ace Pharmaceutics  "      0   8))
	  :do (let ((stock-hist (dm:hull 'stock-history)))
			(setf (dm:field stock-hist "stock") (dm:id (ensure-stock (str:trim (first stock))))
				  (dm:field stock-hist "price") (second stock)
				  (dm:field stock-hist "round") (third stock))
			(dm:insert stock-hist))))
