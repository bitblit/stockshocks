(in-package #:cl-user)
(asdf:defsystem #:stockshocks
  :version "0.0.0"
  :defsystem-depends-on (:radiance)
  :class "radiance:virtual-module"
  :depends-on (:str
			   :cl-json
			   (:interface :database)
			   :i-postmodern
			   :r-data-model
			   :cl-redis
			   (:interface :ban)
			   :crypto-shortcuts
			   :dexador)
  :components ((:file "module")
			   (:file "json")
			   (:file "main")
			   (:file "api")))
