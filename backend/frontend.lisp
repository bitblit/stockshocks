(in-package #:stockshocks)

;; Maybe this can be used later?
;; '((:NAME "lol" :PRICE 10) (:NAME "policemen" :PRICE 911))
;; (mapcar (lambda (stock) 
;; 				   (loop :with list := '()
;; 						 :for field :in (mapcar (lambda (x) (intern (str:upcase x) :keyword)) 
;; 												(dm:fields (dm:hull 'stocks)))
;; 						 :do (a:appendf list (list field (dm:field stock field)))
;; 						 :finally (return list)))
;; 				 (dm:get 'stocks (db:query :all)))

(r:define-page example "/example" ()
  (setf (r:content-type r:*response*) "text/plain")
  "We love Radiance!!! :)")

;; (r:define-page stocks "stockshocks/stocks" (:clip "main.ctml")
;;   (let ((fields (dm:fields (dm:hull 'stocks))))
;; 	(r-clip:process
;; 	 T
;; 	 :name "Stocks"
;; 	 :fields (mapcar #'str:capitalize fields)
;; 	 :data (mapcar (lambda (stock)
;; 					 (loop :for field :in fields
;; 						   :collect (dm:field stock field)))
;; 				   (dm:get 'stocks (db:query :all))))))

(r:define-page stocks "stockshocks/users" (:clip "main.ctml")
  (let ((fields (delete "password" (dm:fields (dm:hull 'users)) :test #'string=)))
	(r-clip:process
	 T
	 :name "Users"
	 :fields (mapcar #'str:capitalize fields)
	 :data (mapcar (lambda (user)
					 (loop :for field :in fields
						   :collect (dm:field user field)))
				   (dm:get 'users (db:query :all))))))

;; (r:define-page stocks "stockshocks/teams" (:clip "main.ctml")
;;   (let ((fields (dm:fields (dm:hull 'teams))))
;; 	(r-clip:process
;; 	 T
;; 	 :name "Teams"
;; 	 :fields (mapcar #'str:capitalize fields)
;; 	 :data (mapcar (lambda (team)
;; 					 (loop :for field :in fields
;; 						   :collect (dm:field team field)))
;; 				   (dm:get 'teams (db:query :all))))))

(r:define-page profile-view "/u/(.*)" (:uri-groups (id))
  (setf (r:content-type r:*response*) "text/plain")
  (format nil "Hello ~a, this is for you~%~%Niggas be looking at Greko (greko)~%shotup in all o' them hatos (hatos)" id))
