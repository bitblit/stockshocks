import type { PageServerLoad } from './$types';
import { redirect } from '@sveltejs/kit';

let loadTime = 0;

export const load: PageServerLoad = (event) => {
	loadTime = Date.now();
};

export const actions: Actions = {
	default: async ({ request, cookies, locals }) => {
		const data = await request.formData();
		const honeypot = data.get('pot');
		let elapsedTime = Date.now() - loadTime;
		if (honeypot !== '' || elapsedTime < 1000) {
			console.log('CRINGE ALERT!!!');
			// const banning = await fetch(`http://127.0.0.1:8080/api/ratelimit/`);
			return;
		}
		const username = data.get('username');
		const password = encodeURIComponent(data.get('password'));
		const team = encodeURIComponent(data.get('team'));
		const escapedUsername = encodeURIComponent(username);
		let response = await fetch(
			`http://127.0.0.1:8080/api/user/create?username=${escapedUsername}&password=${password}&team_name=${team}`
		).then((response) => response.json());

		if (!!response.status && response.status !== 500) {
			// console.log('SIGNUP AUTH PASSED');
			response = await fetch(
				`http://127.0.0.1:8080/api/user/login?username=${escapedUsername}&password=${password}`
			).then((response) => response.json());
			// console.log('Login after SIGNUP AUTH PASSED');
			cookies.set(
				'login',
				`username=${escapedUsername}&series=${response.data[1]}&token=${response.data[2]}`,
				{
					path: '/',
					maxAge: 60 * 60 * 24 * 10,
					httpOnly: true,
					secure: true
				}
			);
			locals.user = username;
			throw redirect(302, '/');
		} else {
			// console.log('SIGNUP AUTH FAILED >>:((');
		}

		return {
			success: response.status !== 500,
			message: response.message,
			username: locals.user
		};
	}
};
