import type { PageServerLoad, Actions } from './$types';
import { redirect, error, json } from '@sveltejs/kit';
import Headlines from '$lib/headlines.json';

let headlines;

export const load: PageServerLoad = async ({ cookies, locals, depends, parent }) => {
	depends('mydata:round');

	let parentLocals = await parent();

	if (!!locals.user) {
		const currentRoundResponse = await fetch('http://127.0.0.1:8080/api/round/current').then(
			(response) => response.json()
		);

		if (!!currentRoundResponse.status && currentRoundResponse.status !== 500) {
			// console.log('Fetched current round.');
		} else {
			throw error(500, { message: 'Failed fetching current round.' });
		}

		const fetchRoundInfo = async () => {
			const timeResponse = await fetch('http://127.0.0.1:8080/api/next-round-time').then(
				(response) => response.json()
			);
			const roundsResponse = await fetch('http://127.0.0.1:8080/api/rounds').then((response) =>
				response.json()
			);
			if (
				!!timeResponse.status &&
				timeResponse.status !== 500 &&
				!!roundsResponse.status &&
				roundsResponse.status !== 500
			) {
				// console.log(`Fetched round info.`);
			} else {
				throw error(500, { message: 'Failed fetching round info.' });
			}

			return {
				time: new Date(timeResponse.data * 1000),
				currentRound: currentRoundResponse.data
			};
		};

		if (currentRoundResponse.data !== -1) {
			const fetchStockInfo = async () => {
				const stocksResponse = await fetch(`http://127.0.0.1:8080/api/stocks`).then((response) =>
					response.json()
				);
				let ownedStocks = [];
				if (!!stocksResponse.status && stocksResponse.status !== 500) {
					// console.log('Fetched stocks.');
					for (const stock of stocksResponse.data) {
						let stockName = stock.name;
						let escapedStockName = encodeURIComponent(stockName);
						let escapedUser = encodeURIComponent(locals.user);
						const ownedResponse = await fetch(
							`http://127.0.0.1:8080/api/user/owned?user=${escapedUser}&stock=${escapedStockName}`
						).then((response) => response.json());
						if (!!ownedResponse.status && ownedResponse.status !== 500) {
							ownedStocks.push({ stock: stockName, owned: ownedResponse.data });
						} else {
							console.log(ownedResponse);
							// console.log('Could not fetch owned stocks.');
						}
					}
				} else {
					throw error(500, { message: 'Failed fetching stocks.' });
				}

				// console.log(ownedStocks);

				return {
					stocksResponse: stocksResponse,
					ownedStocks: ownedStocks
				};
			};

			const fetchHeadline = async () => {
				const headlinesResponse = new json(Headlines);
				let headlines = await headlinesResponse.json();
				// console.log(headlines);
				let headline = headlines.find((h) => h.round === currentRoundResponse.data);
				// console.log(headline);
				return headline;
			};

			return {
				roundInfo: fetchRoundInfo(),
				headline: fetchHeadline(),
				stockInfo: fetchStockInfo()
			};
		} else {
			return {
				roundInfo: { currentRound: currentRoundResponse.data },
				bro: 101
			};
		}
	}

	// console.log('Not signed in...');

	return {
		status: locals.loginStatus
	};
};

export const actions: Actions = {
	logout: async ({ cookies, locals }) => {
		const response = await fetch(
			`http://127.0.0.1:8080/api/user/logout?username=${locals.user}`
		).then((response) => response.json());

		if (!!response.status && response.status !== 500) {
			// console.log('LOGOUT PASSED');
			await cookies.delete('login');
			locals.user = null;
			throw redirect(302, '/');
		} else {
			console.log('LOGOUT FAILED >>:((');
		}

		locals.logoutStatus = response.status;

		return {
			success: locals.logoutStatus,
			message: response.message,
			username: locals.user
		};
	}
};
