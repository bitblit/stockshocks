import type { PageServerLoad, Actions } from './$types';

export const load: PageServerLoad = ({ locals, params }) => {
	let chartData = {
		labels: ['Sun', 'Mon', 'Tues', 'Wed', 'Thurs', 'Fri', 'Sat'],
		datasets: [
			{
				values: [10, 12, 3, 9, 8, 15, 9]
			}
		]
	};
	return {
		chartData: chartData
	};
};

export const actions: Actions = {
	buy: async ({ locals, params, request }) => {
		// console.log('Reached Buying');
		let status, message;
		let data = await request.formData();
		let buyValue = data.get('buyValue');
		// console.log(buyValue);
		if (!!buyValue) {
			const response = await fetch(
				`http://127.0.0.1:8080/api/user/buy?user=${locals.user}&stock=${params.id}&amount=${buyValue}`
			).then((response) => response.json());
			// console.log(response);
			// chartRef.addDataPoint('Wed', [buyValue])
			return {
				status: response.status,
				message: response.message
			};
		}
	},
	sell: async ({ locals, params, request }) => {
		// console.log('Reached Selling');
		let status, message;
		let data = await request.formData();
		let sellValue = data.get('sellValue');
		if (!!sellValue) {
			// console.log(sellValue);
			const response = await fetch(
				`http://127.0.0.1:8080/api/user/sell?user=${locals.user}&stock=${params.id}&amount=${sellValue}`
			).then((response) => response.json());
			// console.log(response);
			// chartRef.addDataPoint('Wed', [buyValue])
			return {
				status: response.status,
				message: response.message
			};
		}
	}
};
