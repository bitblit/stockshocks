import type { LayoutServerLoad } from './$types';

export const load: LayoutServerLoad = async ({ locals }) => {
	if (!!locals.user) {
		const fetchStats = async () => {
			const balanceResponse = await fetch(
				`http://127.0.0.1:8080/api/user-team?user=${locals.user}`
			).then((response) => response.json());
			const shareResponse = await fetch(`http://127.0.0.1:8080/api/user?user=${locals.user}`).then(
				(response) => response.json()
			);

			if (
				!!balanceResponse.status &&
				balanceResponse.status !== 500 &&
				!!shareResponse.status &&
				shareResponse.status !== 500
			) {
				// console.log('Fetched statistics.');
				// console.log(balanceResponse, shareResponse);
			} else {
				throw error(404, { message: 'Failed fetching statistics.' });
			}

			let balance = balanceResponse.data.balance;
			let share = shareResponse.data.share;

			// const nFormat = new Intl.NumberFormat();
			return {
				balance: balance,
				share: share,
				cashOnHand: balance * share,
				balanceString: '$' + balance.toFixed(1).replace(/(.)(?=(\d{3})+$)/g, '$1,'),
				shareString: (share * 100).toFixed(1) + '%',
				cashOnHandString: '$' + (balance * share).toFixed(1).replace(/(.)(?=(\d{3})+$)/g, '$1,')
			};
		};

		const fetchCurrentRound = async () => {
			const roundResponse = await fetch('http://127.0.0.1:8080/api/round/current').then(
				(response) => response.json()
			);
			if (!!roundResponse.status && roundResponse.status !== 500) {
				// console.log('Fetched current round.');
			} else {
				throw error(404, { message: 'Failed fetching current round.' });
			}

			return roundResponse.data;
		};

		return {
			username: locals.user,
			loginStatus: locals.loginStatus,
			stats: fetchStats(),
			currentRound: fetchCurrentRound()
		};
	}
	return {
		username: locals.user,
		loginStatus: locals.loginStatus
	};
};
