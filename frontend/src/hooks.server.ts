import { redirect, error, type Handle } from '@sveltejs/kit';

export const handle: Handle = async ({ event, resolve }) => {
	// console.log('IN THE HOOK HANDLE');

	if (!event.locals.user && event.locals.logoutStatus !== 500) {
		let loginCookie: String = event.cookies.get('login');
		// console.log(loginCookie);
		if (!loginCookie) {
			event.locals.user = null;
			event.locals.loginStatus = -99;
		} else {
			const response = await fetch(
				`http://127.0.0.1:8080/api/user/cookie-login?${loginCookie}`
			).then((response) => response.json());
			if (!!response.status && response.status !== 500) {
				await event.cookies.set(
					'login',
					loginCookie.replace(/token=.*/g, `token=${response.data[2]}`),
					{
						path: '/',
						maxAge: 60 * 60 * 24 * 10,
						httpOnly: true,
						secure: false // ONLY FOR TESTING
					}
				);
				event.locals.user = event.cookies.get('login').match(/username=(.*?)&/)[1];
			} else {
				console.log('Cookie login failed :(');
				console.log(response.message);
				// throw error(500, {
				// 	message: 'Login failed. Are you using two devices to login to the same account?'
				// });
			}
			event.locals.loginStatus = response.status;
		}
	}

	if (event.url.pathname.startsWith('/stock') && !!event.locals.user) {
		console.log('Spidey-senses activated');
		const currentRound = await fetch('http://127.0.0.1:8080/api/round/current').then((response) =>
			response.json()
		).data;
		if (currentRound === -1 || currentRound === 0) {
			throw redirect('/'); // TODO: Make this work lmao
		}
	}

	const response = await resolve(event);

	return response;
};
