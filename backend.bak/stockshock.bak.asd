(asdf:defsystem "stockshocks.bak"
  :version "0.1.0"
  :author "Hamza Shahid"
  :license "Proprietary"
  :depends-on ((:interface :database)
			   :r-data-model)
  :components ((:module "src"
                :components
                ((:file "package")
				 (:file "json")
				 (:file "main"))))
  :description "Investing Simulator for 4 day event"
  :in-order-to ((asdf:test-op (asdf:test-op "stockshocks.bak/tests"))))

(asdf:defsystem "stockshocks.bak/tests"
  :author "Hamza Shahid"
  :license "Proprietary"
  :depends-on ("stockshocks"
               "rove")
  :components ((:module "tests"
                :components
                ((:file "main"))))
  :description "Test system for stockshocks"
  :perform (asdf:test-op (op c) (symbol-call :rove :run c)))
